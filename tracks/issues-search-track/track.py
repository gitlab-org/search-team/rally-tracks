import random

def random_search_term(track, params, **kwargs):
    # choose a suitable index: if there is only one defined for this track
    # choose that one, but let the user always override index and type.
    if len(track.indices) == 1:
        default_index = track.indices[0].name
    else:
        default_index = "_all"

    index_name = params.get("index", default_index)
    project_id = params.get("project-id", 6)
    search_terms = params.get("search-terms", "*")

    i = random.randint(0, len(search_terms)-1)
    term = search_terms[i] 

    # you must provide all parameters that the runner expects
    return {
      "query": {
        "bool": {
          "must": [
            {
              "simple_query_string": {
                "_name": "issue:match:search_terms",
                "fields": [
                  "iid^3",
                  "title^2",
                  "description"
                ],
                "query": term,
                "lenient": True,
                "default_operator": "and"
              }
            }
          ],
          "filter": [
            {
              "terms": {
                "_name": "issue:authorized:project",
                "project_id": [
                  project_id
                ]
              }
            }
          ]
        }
      },
      "highlight": {
        "fields": {
          "iid": {},
          "title": {},
          "description": {}
        },
        "number_of_fragments": 0,
        "pre_tags": [
          "gitlabelasticsearch→"
        ],
        "post_tags": [
          "←gitlabelasticsearch"
        ]
      },
      "index": index_name,
      "cache": params.get("cache", False)
    }

def register(registry):
    registry.register_param_source("search-project-source", random_search_term)