# Search Team Rally Tracks

This project stores [Rally tracks](https://esrally.readthedocs.io/en/stable/index.html) used for benchmarking the Elasticsearch cluster that supports the [Advanced Search](https://docs.gitlab.com/ee/user/search/advanced_search.html) feature.

## Getting started

### Prerequisites

The instructions below follow the documentation for running [Rally on Docker](https://esrally.readthedocs.io/en/stable/docker.html). Docker must be installed.

### Setup

Get a local instance of Rally going to run your first race.

1. Clone the repository 

```shell
git clone git@gitlab.com:gitlab-org/search-team/rally-tracks.git ~/rally-tracks
```

2. Change directories into the cloned repositgit clone git@gitlab.com:gitlab-org/search-team/rally-tracks.git ~/rally-tracks

```console
cd ~/rally-tracks
```

3. Run Rally using Docker and map directories in the Docker container to the local directories. 
_Note: These instructions were written while using [Colima on macOS](https://github.com/abiosoft/colima)._

```console
docker run --rm  -v "$(pwd)"/data:/rally/.rally -v "$(pwd)"/tracks:/tracks -it --entrypoint /bin/bash elastic/rally
```

4. List the tracks. _Note: Run from inside Docker container_

```console
 esrally list tracks --track-path=/tracks/issues-search-track/
```

## Running a race

**Run tests against a local Elasticsearch index. DO NOT run against Production!**

```console
esrally race --track-path=/tracks/issues-search-track/ --pipeline=benchmark-only --target-hosts=host.docker.internal:9200 --track-params="p_cluster_health:'yellow',p_project_id:6" 
```

A few notes about the [command line flags](https://esrally.readthedocs.io/en/stable/command_line_reference.html?highlight=target-hosts#command-line-flags) used:

- `--track-path` - Tell Rally which directory the track is stored
- `--pipeline` - [`benchmark-only`](https://esrally.readthedocs.io/en/stable/pipelines.html) assumes an already running Elasticsearch instance and is the only pipeline supported for Docker
- `--target-hosts` - Elasticsearch host(s)
- `--track-params` - Override params in the track (if not overridden defaults will be used).
  - `p_cluster_health` allows you to set the cluster health required before starting the test. In the development instance of Elasticsearch used while making this track, the health is always `yellow` status. 
  - `p_project_id` allows you to switch the project used in the search query.

### Output from race

Output from a race is displayed in the console after the race is completed.

<details>

<summary>Expand for example race output</summary>

```console


    ____        ____
   / __ \____ _/ / /_  __
  / /_/ / __ `/ / / / / /
 / _, _/ /_/ / / / /_/ /
/_/ |_|\__,_/_/_/\__, /
                /____/

[INFO] Race id is [9896c41a-6166-4ae7-adad-7fbd86ee6bdb]
[INFO] Racing on track [issues-search-track] and car ['external'] with version [8.2.0].

Running delete                                                                 [100% done]
Running create                                                                 [100% done]
Running wait-for-cluster                                                       [100% done]
Running bulk-index                                                             [100% done]
Running query-match-all                                                        [100% done]
Running query-project-level                                                    [100% done]
Running project-level-search                                                   [100% done]

------------------------------------------------------
    _______             __   _____
   / ____(_)___  ____ _/ /  / ___/_________  ________
  / /_  / / __ \/ __ `/ /   \__ \/ ___/ __ \/ ___/ _ \
 / __/ / / / / / /_/ / /   ___/ / /__/ /_/ / /  /  __/
/_/   /_/_/ /_/\__,_/_/   /____/\___/\____/_/   \___/
------------------------------------------------------

|                                                         Metric |                 Task |          Value |   Unit |
|---------------------------------------------------------------:|---------------------:|---------------:|-------:|
|                     Cumulative indexing time of primary shards |                      |    0.105633    |    min |
|             Min cumulative indexing time across primary shards |                      |    0           |    min |
|          Median cumulative indexing time across primary shards |                      |    0           |    min |
|             Max cumulative indexing time across primary shards |                      |    0.0261333   |    min |
|            Cumulative indexing throttle time of primary shards |                      |    0           |    min |
|    Min cumulative indexing throttle time across primary shards |                      |    0           |    min |
| Median cumulative indexing throttle time across primary shards |                      |    0           |    min |
|    Max cumulative indexing throttle time across primary shards |                      |    0           |    min |
|                        Cumulative merge time of primary shards |                      |    0           |    min |
|                       Cumulative merge count of primary shards |                      |    0           |        |
|                Min cumulative merge time across primary shards |                      |    0           |    min |
|             Median cumulative merge time across primary shards |                      |    0           |    min |
|                Max cumulative merge time across primary shards |                      |    0           |    min |
|               Cumulative merge throttle time of primary shards |                      |    0           |    min |
|       Min cumulative merge throttle time across primary shards |                      |    0           |    min |
|    Median cumulative merge throttle time across primary shards |                      |    0           |    min |
|       Max cumulative merge throttle time across primary shards |                      |    0           |    min |
|                      Cumulative refresh time of primary shards |                      |    0.0109833   |    min |
|                     Cumulative refresh count of primary shards |                      |   98           |        |
|              Min cumulative refresh time across primary shards |                      |    0           |    min |
|           Median cumulative refresh time across primary shards |                      |    0           |    min |
|              Max cumulative refresh time across primary shards |                      |    0.00485     |    min |
|                        Cumulative flush time of primary shards |                      |    0.0505833   |    min |
|                       Cumulative flush count of primary shards |                      |   41           |        |
|                Min cumulative flush time across primary shards |                      |    0           |    min |
|             Median cumulative flush time across primary shards |                      |    0           |    min |
|                Max cumulative flush time across primary shards |                      |    0.0106      |    min |
|                                        Total Young Gen GC time |                      |    0.433       |      s |
|                                       Total Young Gen GC count |                      |  107           |        |
|                                          Total Old Gen GC time |                      |    0           |      s |
|                                         Total Old Gen GC count |                      |    0           |        |
|                                                     Store size |                      |    0.140265    |     GB |
|                                                  Translog size |                      |    2.10013e-06 |     GB |
|                                         Heap used for segments |                      |    0           |     MB |
|                                       Heap used for doc values |                      |    0           |     MB |
|                                            Heap used for terms |                      |    0           |     MB |
|                                            Heap used for norms |                      |    0           |     MB |
|                                           Heap used for points |                      |    0           |     MB |
|                                    Heap used for stored fields |                      |    0           |     MB |
|                                                  Segment count |                      |   87           |        |
|                                    Total Ingest Pipeline count |                      |    0           |        |
|                                     Total Ingest Pipeline time |                      |    0           |      s |
|                                   Total Ingest Pipeline failed |                      |    0           |        |
|                                                 Min Throughput |           bulk-index | 7543.99        | docs/s |
|                                                Mean Throughput |           bulk-index | 7543.99        | docs/s |
|                                              Median Throughput |           bulk-index | 7543.99        | docs/s |
|                                                 Max Throughput |           bulk-index | 7543.99        | docs/s |
|                                        50th percentile latency |           bulk-index | 1019.5         |     ms |
|                                       100th percentile latency |           bulk-index | 1215.96        |     ms |
|                                   50th percentile service time |           bulk-index | 1019.5         |     ms |
|                                  100th percentile service time |           bulk-index | 1215.96        |     ms |
|                                                     error rate |           bulk-index |    0           |      % |
|                                                 Min Throughput |      query-match-all |   99.95        |  ops/s |
|                                                Mean Throughput |      query-match-all |   99.96        |  ops/s |
|                                              Median Throughput |      query-match-all |   99.96        |  ops/s |
|                                                 Max Throughput |      query-match-all |   99.97        |  ops/s |
|                                        50th percentile latency |      query-match-all |   12.4612      |     ms |
|                                        90th percentile latency |      query-match-all |   18.9392      |     ms |
|                                        99th percentile latency |      query-match-all |   27.1148      |     ms |
|                                      99.9th percentile latency |      query-match-all |   48.8607      |     ms |
|                                       100th percentile latency |      query-match-all |   73.4798      |     ms |
|                                   50th percentile service time |      query-match-all |    8.76112     |     ms |
|                                   90th percentile service time |      query-match-all |   13.7907      |     ms |
|                                   99th percentile service time |      query-match-all |   20.1563      |     ms |
|                                 99.9th percentile service time |      query-match-all |   42.0179      |     ms |
|                                  100th percentile service time |      query-match-all |   67.3317      |     ms |
|                                                     error rate |      query-match-all |    0           |      % |
|                                                 Min Throughput |  query-project-level |   99.85        |  ops/s |
|                                                Mean Throughput |  query-project-level |   99.9         |  ops/s |
|                                              Median Throughput |  query-project-level |   99.9         |  ops/s |
|                                                 Max Throughput |  query-project-level |   99.93        |  ops/s |
|                                        50th percentile latency |  query-project-level |   11.8828      |     ms |
|                                        90th percentile latency |  query-project-level |   18.6032      |     ms |
|                                        99th percentile latency |  query-project-level |   59.267       |     ms |
|                                      99.9th percentile latency |  query-project-level |  304.595       |     ms |
|                                       100th percentile latency |  query-project-level |  361.154       |     ms |
|                                   50th percentile service time |  query-project-level |    7.31496     |     ms |
|                                   90th percentile service time |  query-project-level |   13.016       |     ms |
|                                   99th percentile service time |  query-project-level |   23.8773      |     ms |
|                                 99.9th percentile service time |  query-project-level |  120.627       |     ms |
|                                  100th percentile service time |  query-project-level |  150.706       |     ms |
|                                                     error rate |  query-project-level |    0           |      % |
|                                                 Min Throughput | project-level-search |   99.98        |  ops/s |
|                                                Mean Throughput | project-level-search |  100.01        |  ops/s |
|                                              Median Throughput | project-level-search |  100           |  ops/s |
|                                                 Max Throughput | project-level-search |  100.04        |  ops/s |
|                                        50th percentile latency | project-level-search |   11.3348      |     ms |
|                                        90th percentile latency | project-level-search |   16.1746      |     ms |
|                                        99th percentile latency | project-level-search |   24.427       |     ms |
|                                      99.9th percentile latency | project-level-search |   40.245       |     ms |
|                                       100th percentile latency | project-level-search |   53.3966      |     ms |
|                                   50th percentile service time | project-level-search |    7.85342     |     ms |
|                                   90th percentile service time | project-level-search |   11.6846      |     ms |
|                                   99th percentile service time | project-level-search |   19.2312      |     ms |
|                                 99.9th percentile service time | project-level-search |   34.6296      |     ms |
|                                  100th percentile service time | project-level-search |   44.3613      |     ms |
|                                                     error rate | project-level-search |    0           |      % |


---------------------------------
[INFO] SUCCESS (took 538 seconds)
---------------------------------
```

</details>


## Creating a custom track

This track was [created from data in an existing local Elasticsearch cluster](https://esrally.readthedocs.io/en/stable/adding_tracks.html) run by the [gitlab-development-kit](https://gitlab.com/gitlab-org/gitlab-development-kit/). _Note: Run from inside Docker container_

Example: 

```console
 esrally create-track --track=notes-track-example --target-hosts=host.docker.internal:9200 --indices="gitlab-development-notes" --output-path=/tracks
 ```

<details>

<summary>Expand for example track creation output</summary>

```sh

    ____        ____
   / __ \____ _/ / /_  __
  / /_/ / __ `/ / / / / /
 / _, _/ /_/ / / / /_/ /
/_/ |_|\__,_/_/_/\__, /
                /____/

[INFO] Connected to Elasticsearch cluster [terrichus-MBP.localdomain] version [8.2.0].

Extracting documents for index [gitlab-development-not...     1000/1000 docs [100.0% done]
Extracting documents for index [gitlab-development-n...     47404/47404 docs [100.0% done]

[INFO] Track notes-track-example has been created. Run it with: esrally --track-path=/tracks/notes-track-example

--------------------------------
[INFO] SUCCESS (took 19 seconds)
--------------------------------
```

</details>

## Debugging queries in operations

There are two recommended options to check [queries and responses](https://esrally.readthedocs.io/en/stable/recipes.html#checking-queries-and-responses)

1. Set the log level for the Elasticsearch client to DEBUG in the `logging.json` file. If you've followed the setup guide above, the file should be located at `/rally-tracks/data/logging.json`. The logs will show up in `/rally/logs/rally.log` when you are running races.

```json
  "loggers": {
    "elasticsearch": {
      "handlers": ["rally_log_handler"],
      "level": "DEBUG",
      "propagate": false
    },
    "rally.profile": {
      "handlers": ["rally_profile_handler"],
      "level": "INFO",
      "propagate": false
    }
  }
 ```

2. Enable `assertions` on the operation and execute the race with the `--enable-assertions` command line flag
